# gitとは

### 参考文献

* Gitとは - IT用語辞典
https://e-words.jp/w/Git.html

なんとなくわかればいいです！

# github

**「GitHub」** とは、バージョン管理システムの **「Git」** を利用して、ソフトウェア開発プロジェクトのソースコード管理を共有することができる **Web サービス** です。  
プログラムの変更が発生する場合、必須の知識になります。


#

<details>
<summary><span style="font-size: 180%"><strong>
1.「GitHub」のアカウントを作成する方法
</strong></span></summary>

### 1-1 GitHub にアクセスしてアカウント情報を登録する

- 「GitHub」の公式サイト (https://github.com/) にアクセス   ※写真と全く同じとは限りません

- メールアドレスを入力し「Sign up for GitHub」をクリック（Gmailが望ましい。未所持の場合は[コチラ](https://accounts.google.com/signup/v2/webcreateaccount?continue=https%3A%2F%2Faccounts.google.com%2FManageAccount%3Fnc%3D1&hl=ja&flowName=GlifWebSignIn&flowEntry=SignUp)から無料で登録）

![登録画面](https://user-images.githubusercontent.com/92492715/144171003-19b170aa-0838-41b2-9a75-406c80b5f177.png "登録画面")

- 先ほど入力したアドレスが表示されていることが確認できたら「Continue」をクリック
![](https://user-images.githubusercontent.com/92492715/144172204-75a11062-407a-469e-87ca-e2dd8feb7a8e.png "アドレス確認")

- パスワードを決める　※8文字以上必須
![](https://user-images.githubusercontent.com/92492715/144173474-dd4f804c-366f-49bf-a47c-38502c26d05d.png "パスワード")

- 好きなユーザー名を決めたら「Continue」をクリック
![](https://user-images.githubusercontent.com/92492715/144173989-7f4582aa-8732-46fe-9f07-ea10515d0220.png "ユーザー名")

- 製品のアップデートやお知らせをメールで受け取るか聞かれているので「y」(yes) か「n」(no) 好きなほうを入力後、「Continue」をクリック
![](https://user-images.githubusercontent.com/92492715/144174492-a7d33131-e7c7-44cf-a5b8-86da496027ff.png "メール受け取り")

- 「検証する」をクリックして簡単な質問（〇〇の画像はどれか等）に答える
![](https://user-images.githubusercontent.com/92492715/144175138-ad617066-712c-474d-83a8-e1df63edd31b.png "検証")

- ✅の表示を確認後、「Create account」をクリック
![](https://user-images.githubusercontent.com/92492715/144175466-cd0c63db-f3fc-44eb-bda0-5036d0bc6e9d.png "アカウント作成")

- 登録したアドレスにメールが届くので、記載された８桁の数字を入力する
![](https://user-images.githubusercontent.com/92492715/144177119-e94fb99a-e617-41bf-833f-12335f498c70.png "コード入力")

- 色々聞かれているがとりあえず「Just me」にチェックを入れて下の「Continue」をクリックする
![](https://user-images.githubusercontent.com/92492715/144177506-e8a6f71e-f14d-4cec-b8d5-133b48ca7335.png "いろいろ")

- そのまま「Continue」をクリックする
![](https://user-images.githubusercontent.com/92492715/144177665-70308ad6-b6fb-4b90-9c60-a0afcbc8ac8c.png "そのまま")

- 無償で使える「Contunue for free」をクリックする
![](https://user-images.githubusercontent.com/92492715/144177684-59f93577-6f06-4656-ba9d-99630d36fad8.png "無償選択")

- すごいムービーが始まったら成功
![](https://user-images.githubusercontent.com/92492715/144177766-c4803afb-4290-409c-b176-91e810a632ff.png "成功")

</details>

#

<details>
<summary><span style="font-size: 180%"><strong>
2.GitHubを使う上で知っておきたい事前知識
</strong></span></summary>
